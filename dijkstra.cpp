#include <climits>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> pii;
typedef vector< pii > vii;
const int INF = INT_MAX;

vii *G;
vi Dist;
int n, m;

int Dijkstra(int source, int dest) {
    priority_queue<pii, vector<pii>, greater<pii> > Q;
    Dist.assign(n,INF);
    Dist[source] = 0;
    Q.push({0,source});
    while(!Q.empty()){
        int u = Q.top().second;
        Q.pop();
        for(auto &c : G[u]){
            int v = c.first;
            int w = c.second;
            if(Dist[v] > Dist[u] + w){
                Dist[v] = Dist[u] + w;
                Q.push({Dist[v], v});
            }
        }
    }
    return Dist[dest];
}

int main(){
    int a, b, w;
    cin >> n >> m;
    G = new vii[n + 1];

    for(int i = 0; i < m; ++i){
        cin >> a >> b >> w;
        G[a - 1].push_back({b - 1, w});
    }
    cout << Dijkstra(0, n - 1) << endl;
    return 0;
}
