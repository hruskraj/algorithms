#include <cstdlib>
#include <cmath>

using namespace std;

struct Circle{
    double x, y, r;
};

bool intersection(Circle & A, Circle & B, Circle & P1, Circle & P2){
    double d = hypot(B.x - A.x, B.y - A.y);
    if(d <= A.r + B.r && d >= abs(B.r - A.r)){
        double ex = (B.x - A.x) / d;
        double ey = (B.y - A.y) / d;

        double x = (A.r * A.r - B.r * B.r + d * d) / (2 * d);
        double y = sqrt(A.r * A.r - x * x);

        P1.x = A.x + x * ex - y * ey;
        P1.y = A.y + x * ey + y * ex;

        P2.x = A.x + x * ex + y * ey;
        P2.y = A.y + x * ey - y * ex;

        return true;
    }
    return false;
}

int main(){
    return 0;
}
