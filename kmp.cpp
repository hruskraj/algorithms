void computeLPS(string & ptr){
    int len = 0, i = 1;
    lps[0] = 0;
    while(i < m){
        if(ptr[i] == ptr[len])
            lps[i++] = ++len;
        else{
            if(len != 0)
                len = lps[len - 1];
            else
                lps[i++] = 0;
        }
    }
}

vector<int> KMP(string & txt, string & ptr){
    n = txt.size();
    m = ptr.size();
    int i = 0, j = 0;
    lps = new int[m];
    vector<int> found;

    computeLPS(ptr);

    while(i < n){
        if(txt[i] == ptr[j]){
            ++i; ++j;
        }
        if(j == m){
            found.push_back(i - j);
            j = lps[j - 1];
        }
        else if(i < n && ptr[j] != txt[i]){
            if(j != 0)
                j = lps[j - 1];
            else
                i = i + 1;
        }
    }
    delete [] lps;
    return found;
}