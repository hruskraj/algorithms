#include <iostream>

using namespace std;
bool f(int m){
    if(m >= 4)
        return true;
    return false;
}

int main(){
    ios::sync_with_stdio(false);
    //invariant f(start) = false, f(end) = true;
    int start = 0, end = 1000002, m;
    while(end - start > 1){
        m = (start + end) >> 1;
        (f(m) ? end : start) = m;
    }
    cout << end << endl;
    return 0;
}
