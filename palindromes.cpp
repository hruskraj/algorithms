string addBoundaries(string & s){
    string out = "~";
    for(int i = 0; i < s.length(); ++i)
        out = out + "#" + s[i];
    out = out + "#$";
    return out;
}

pair<int, int> longestPalindrome(string & s){
    string t = addBoundaries(s);
    int n = t.length(), c = 0, r = 0;
    int * P = new int[n];

    for(int i = 1; i < n - 1; ++i){
        int j = 2 * c - i;
        P[i] = (r > i ? min(r - i, P[j]) : 0);

        while(t[i + 1 + P[i]] == t[i - 1 - P[i]])
            ++P[i];
        if(i + P[i] > r){
            c = i;
            r = i + P[i];
        }
    }

    int maxx = 0, cnt = 0;
    for(int i = 1; i < n - 1; ++i){
        if(P[i] == maxx)
            ++cnt;
        else if(P[i] > maxx){
            maxx = P[i];
            cnt = 1;
        }
    }
    return make_pair(maxx, cnt);
}
